//
//  JahezCustomerTests.swift
//  JahezCustomerTests
//
//  Created by Abdul Rehman on 26/01/2022.
//

import XCTest
@testable import JahezCustomer

class JahezCustomerTests: XCTestCase {
    let viewModel = RestaurantsViewModel()

    func test_Model_Null()
    {
           let instance = RestaurantModel(id: 0,
                          distance:  2.0,
                          rating:  1,
                          hours:   "0501234567",
                          description:  "0501234567",
                          name:   "0501234567",
                          hasOffer:   true,
                          image: URL(string: "https://jahez-other-oniiphi8.s3.eu-central-1.amazonaws.com/1.jpg")!)
        
        XCTAssertNotNil(instance, "mode null test")
     }
    
    func test_List_empty() {

         XCTAssertNotNil(viewModel.restaurants, "restaurant array empty test")
    }
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        try super.setUpWithError()
     }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        try super.tearDownWithError()

    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        // Any test you write for XCTest can be annotated as throws and async.
        // Mark your test throws to produce an unexpected failure when your test encounters an uncaught error.
        // Mark your test async to allow awaiting for asynchronous code to complete. Check the results with assertions afterwards.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        measure {
            // Put the code you want to measure the time of here.
        }
    }

}
