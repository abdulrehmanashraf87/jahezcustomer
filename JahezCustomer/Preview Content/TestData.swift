//
//  TestData.swift
//  JahezCustomer
//
//  Created by Abdul Rehman on 26/01/2022.
//

import Foundation

struct TestData {
	static let restaurant: RestaurantModel = {
		let url = Bundle.main.url(forResource: "Restaurant", withExtension: "json")!
		let data = try! Data(contentsOf: url)
		let decoder = JSONDecoder()
		decoder.dateDecodingStrategy = .secondsSince1970
		return try! decoder.decode(RestaurantModel.self, from: data)
	}()
}
