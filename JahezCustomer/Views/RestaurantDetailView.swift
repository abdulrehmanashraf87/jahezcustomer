//
//  RestaurantDetailView.swift
//  JahezCustomer
//
//  Created by Abdul Rehman on 26/01/2022.
//

import SwiftUI

struct RestaurantDetailView: View {
    var restaurant: RestaurantModel?

    var body: some View {
        ScrollView {
 
            VStack(alignment: .leading) {
                BannerImageView(withURL: restaurant?.image.absoluteString ?? "")
                Divider()
                HStack {
                    Text(restaurant?.name ?? "")
                        .font(.largeTitle)
                     Divider()
                     Text(restaurant?.description ?? "")
                        .font(.subheadline)
                }
                .font(.subheadline)
                .foregroundColor(.secondary)
                Divider()
                HStack(spacing: 8.0) {
                    Text("Hours:" + "\n" + (restaurant?.hours.description)!).font(.footnote)
                        .frame(maxWidth: .infinity)
                    Divider()
                    Text("Rating: " + (restaurant?.rating.description)!).font(.footnote)
                        .frame(maxWidth: .infinity)
                }
                
                Divider()
             }
            .padding()
        }
        .navigationTitle(restaurant?.name ?? "")
        .navigationBarTitleDisplayMode(.inline)
    }
}
 


struct BannerImageView: View {
    @ObservedObject var imageLoader:ImageLoader

    init(withURL url:String) {
        imageLoader = ImageLoader(urlString:url)
    }

    var body: some View {

            Image(uiImage: imageLoader.image ?? UIImage() )
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width:300, height:200).padding(.horizontal).background(Color.clear)
    }
}
 
