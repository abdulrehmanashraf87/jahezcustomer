//
//  RestaurantsView.swift
//  JahezCustomer
//
//  Created by Abdul Rehman on 26/01/2022.
//

import SwiftUI

struct RestaurantsView: View {
	@StateObject private var model = RestaurantsViewModel()
    @State var isFilter : Bool = false

	var body: some View {
        List(model.restaurants.indices) { index in
			if let restaurant = model.restaurants[index] {
                NavigationLink(destination: RestaurantDetailView(restaurant: restaurant)) {
                    Restaurant(position: index + 1, item: restaurant)
                }
			}
		}
		.navigationTitle("Restaurants")
        .toolbar {
            ToolbarItem(placement: .navigationBarLeading) {
                VStack {
                    Button{
                        print("pressed")
                        if self.isFilter{
                            model.fetchRestaurants()
                        }
                        else{
                            model.sortDECbyRate()
                        }
                        self.isFilter.toggle()

                    }
                label: {
                    Image(self.isFilter == true ? "unfilter" : "filter")
                        .resizable()
                        .frame(width: 20, height: 20)
                }
                }
            }
        }
		.onAppear(perform: model.fetchRestaurants)
        .onDisappear(perform: {isFilter = false})
 	}
}

// MARK: - Restaurant
struct Restaurant: View {
	let position: Int
	let title: String
    let url: String
	var body: some View {
		HStack(alignment: .top, spacing: 16.0) {
            HStack {
                ImageView(withURL: url)
               Text(title)
             }

		}
		.padding(.top, 16.0)
	}
}

extension Restaurant {
	init(position: Int, item: RestaurantModel) {
		self.position = position
        title = item.name
        url = item.image.absoluteString
		 
	}
}

struct Badge: View {
	let text: String
	let imageName: String
	
	var body: some View {
		HStack {
			Image(systemName: imageName)
			Text(text)
		}
	}
}
 

struct ImageView: View {
    @ObservedObject var imageLoader:ImageLoader

    init(withURL url:String) {
        imageLoader = ImageLoader(urlString:url)
    }

    var body: some View {

            Image(uiImage: imageLoader.image ?? UIImage() )
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width:70, height:70)
    }
}
 
