//
//  JahezCustomerApp.swift
//  JahezCustomer
//
//  Created by Abdul Rehman on 26/01/2022.
//

import SwiftUI

@main
struct JahezCustomerApp: App {
	var body: some Scene {
		WindowGroup {
			NavigationView {
                RestaurantsView()
			}
		}
	}
}
