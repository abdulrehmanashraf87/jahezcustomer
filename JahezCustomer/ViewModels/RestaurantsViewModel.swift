//
//  RestaurantsViewModel.swift
//  JahezCustomer
//
//  Created by Abdul Rehman on 26/01/2022.
//

import Foundation

class RestaurantsViewModel: ObservableObject {
    @Published var restaurants: [RestaurantModel?] = Array(repeating: nil, count: 50)

	func fetchRestaurants() {
		let url = URL(string: "https://jahez-other-oniiphi8.s3.eu-central-1.amazonaws.com/restaurants.json")!
		let request = APIRequest(url: url)
		request.perform { [weak self] (restaurants: [RestaurantModel]?) -> Void in
            guard let restaurants = restaurants?.prefix(50) else { return }
            for (index, restaurants) in restaurants.enumerated() {
                     self?.restaurants[index] = restaurants
             }
        }
	}
    
    func sortASCbyRate() {
        restaurants = restaurants.sorted(by: { ($0?.distance ?? 0) < ($1?.distance ?? 0) })
    }
    func sortDECbyRate() {
        restaurants = restaurants.sorted(by: { ($0?.distance ?? 0) > ($1?.distance ?? 0) })
    }
}

