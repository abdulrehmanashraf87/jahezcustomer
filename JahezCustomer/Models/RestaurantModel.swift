//
//  RestaurantModel.swift
//  JahezCustomer
//
//  Created by Abdul Rehman on 26/01/2022.
//

import Foundation
struct RestaurantModel: Decodable, Hashable {
	let id: Int
	let distance: Float
	let rating: Int
    let hours: String
	let description: String
	let name: String
	let hasOffer: Bool
	let image: URL
}
 
